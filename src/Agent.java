import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Agent {

    public String nom;
    public String image;
    public String html;
    public List<String> materiel = new ArrayList<>();

    public Agent(String nom, String image){

        this.nom = nom;
        this.image = image;


        try {
            File myObj = new File("src/"+ this.nom + ".txt");
            Scanner myReader = new Scanner(myObj);

            int ligne = 5;
            int current = 1;

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (current > ligne){

                    this.materiel.add(data);

                }
                current++;

                this.html = this.GenerateHTML();

            }

    } catch (FileNotFoundException e) {
                e.printStackTrace();
        }
    }

        public String GenerateHTML(){
        String page = "";

        page += "<!DOCTYPE html>\n" +
                "<html lang=\"fr\">\n" +
                "  <head>\n" +
                "    <!-- Meta -->\n" +
                "    <meta charset=\"UTF-8\" />\n" +
                "    <meta content=\"Go Securi\" property=\"og:title\" />\n" +
                "    <meta content=\"Go Securi\" property=\"og:site_name\" />\n" +
                "    <meta\n" +
                "      content=\"Go Securi, protection et surveillance\"\n" +
                "      property=\"og:description\"\n" +
                "    />\n" +
                "    <meta content=\"Go Securi, protection et surveillance\" name=\"description\" />\n" +
                "\n" +
                "    <!-- Link -->\n" +
                "    <link rel=\"icon\" type=\"image/png\" href=\"../img/securi.png\" />\n" +
                "    <link rel=\"stylesheet\" href=\"../css/style.css\" />\n" +
                "    <link rel=\"stylesheet\" href=\"../css/toastr.css\" />\n" +
                "\n" +
                "    <!-- Script -->\n" +
                "    <script src=\"../js/jquery.js\"></script>\n" +
                "    <script src=\"../js/toastr.js\"></script>\n" +
                "    <script src=\"../js/script.js\"></script>";

        page += "    <!-- Title -->\n" +
                "    <title>" + this.nom + "'s card</title>\n" +
                "  </head>";

        page += "  <body>\n" +
                "    <!-- Header -->\n" +
                "    <header>\n" +
                "      <h1>" + this.nom + "</h1>\n" +
                "    </header>\n" +
                "\n" +
                "    <!-- Main -->\n" +
                "    <main>\n" +
                "      <!-- Loading -->\n" +
                "      <section>\n" +
                "        <h2>/</h2>\n" +
                "        <span></span>\n" +
                "      </section>\n" +
                "\n" +
                "      <!-- List of stuff -->\n" +
                "      <div id=\"data\">\n" +
                "        <img src=\"../img/"+ this.nom + ".jpg\" alt=\"\" />\n" +
                "        <div id=\"data-stuff\">\n" +
                "          <ul>\n";

            for (String m: this.materiel
                 ) {

                page += "      <li><input type=\"checkbox\"/>" + m + "</li>\n";
            }

            page +=    "          </ul>\n" +
                "        </div>\n" +
                "      </div>\n" +
                "    </main>\n" +
                "\n" +
                "    <!-- Footer -->\n" +
                "    <footer>\n" +
                "      <small>Copyright Go Securi</small>\n" +
                "    </footer>\n" +
                "  </body>\n" +
                "</html>";
        return page;
    }

}
