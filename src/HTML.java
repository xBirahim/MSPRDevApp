import java.util.List;

public class HTML {

    public List<Agent> agents;

    public String header =
            "<!DOCTYPE html>" +
                    "\n<html lang=\"fr\"><head>\n" +
                    "    <!-- Meta -->\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta content=\"Go Securi\" property=\"og:title\">\n" +
                    "    <meta content=\"Go Securi\" property=\"og:site_name\">\n" +
                    "    <meta content=\"Go Securi, protection et surveillance\" property=\"og:description\">\n" +
                    "    <meta content=\"Go Securi, protection et surveillance\" name=\"description\">\n" +
                    "\n" +
                    "    <!-- Link -->\n" +
                    "    <link rel=\"icon\" type=\"image/png\" href=\"./img/securi.png\">\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/style.css\">\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/toastr.css\">\n" +
                    "\n" +
                    "    <!-- Script -->\n" +
                    "    <script src=\"js/jquery.js\"></script>\n" +
                    "    <script src=\"js/toastr.js\"></script>\n" +
                    "    <script src=\"js/script.js\"></script>\n" +
                    "\n" +
                    "    <!-- Title -->\n" +
                    "    <title>Go Securi</title>\n" +
                    "  </head>\n" +
                    "\n";

    public String body = "\n<body>\n" +
            "    <!-- Header -->\n" +
            "    <header>\n" +
            "      <h1>Liste des agents</h1>\n" +
            "    </header>\n" +
            "\n" +
            "    <!-- Main -->\n" +
            "    <main>\n" +
            "      <!-- Loading -->\n" +
            "      \n" +
            "\n" +
            "      <!-- List of agents -->\n" +
            "      <div id=\"agents\">";


    public String footer =
            "\n      </div>\n" +
                    "    </main>\n" +
                    "</body>" +
                    "\n" +
                    "    <!-- Footer -->\n" +
                    "    <footer>\n" +
                    "      <small>Copyright Go Securi</small>\n" +
                    "    </footer>\n" +
                    "  \n" +
                    "\n" +
                    "</html>";

    public String content = "";


    public HTML(List<Agent> agents){

        this.agents = agents;
        addLink();

        this.content = header + body + footer;

    }

    public void addLink(){

        for (Agent agent : this.agents
        ) {
            body += "\n<a href=http://51.79.145.228/agents/" + agent.nom +".html>" + agent.nom + "</a>";
        }

    }

}
