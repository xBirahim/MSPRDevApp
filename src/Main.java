import java.io.*;
import java.util.*;


public class Main {

    public static void main(String[] args) {

        List<Agent> liste = new ArrayList<>();

        try {
            File myObj = new File("src/staff.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                liste.add(new Agent(data, data + "Image"));
            }

            HTML indexpage = new HTML(liste);

            for (Agent a : liste
                 ) {

                String filePath = "src/pages/agents/" + a.nom + ".html";

                try {
                    File file = new File(filePath);
                    boolean isCreated = file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                FileWriter myWriter = new FileWriter(filePath);
                myWriter.write(a.html);
                myWriter.close();

            }

            String filePath = "src/pages/index.html";

            try {
                File file = new File(filePath);
                boolean isCreated = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            FileWriter myWriter = new FileWriter(filePath);
            myWriter.write(indexpage.content);
            myWriter.close();
            myReader.close();

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
